import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g1 = new Graph ("G1");
      g1.createRandomSimpleGraph (3, 3);
      System.out.println (g1);
      System.out.println(g1.topolSort("v3", "v1"));


//      Represent longest path as a String for better realization:

//      Graph g1 = new Graph ("G1");
//      g1.createRandomSimpleGraph (5, 7);
//      System.out.println (g1);
//      System.out.println(g1.topolSort("v5", "v2"));
//
//      Graph g2 = new Graph ("G2");
//      g2.createRandomSimpleGraph (10, 12);
//      System.out.println (g2);
//      System.out.println(g2.topolSort("v10", "v4"));
//
//      Graph g3 = new Graph ("G3");
//      g3.createRandomSimpleGraph (5, 10);
//      System.out.println (g3);
//      System.out.println(g3.topolSort("v5", "v1"));
//
//      Graph g4 = new Graph ("G4");
//      g4.createRandomSimpleGraph (4, 4);
//      System.out.println (g4);
//      System.out.println(g4.topolSort("v1", "v4"));
//
//      Graph g5 = new Graph ("G5");
//      g5.createRandomSimpleGraph (4, 4);
//      System.out.println (g5);
//      System.out.println(g5.topolSort("v5", "v2"));
//
//      Graph g6 = new Graph ("G6");
//      g6.createRandomSimpleGraph (2000, 2000);
//      System.out.println (g6);
//      System.out.println(g6.topolSort("v2000", "v1"));


   }

   /** Vertex class for representing vertices in the graph. */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public int getVInfo() {
         return info;
      }

      public void setVInfo(int i) {
         this.info = i;
      }

      public boolean hasNext() {
         return next != null;
      }

      public Arc remove() {
          Arc arc = first;
          this.first.is_used = true;
          return arc;
      }

      public Vertex next() {
         return next;
      }

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private boolean is_used = false; // instead of remove

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public boolean hasNext() {
         return next != null;
      }

      public Vertex getToVert() { // get target from arc (Vertex)
         return target;
      }

      public Arc next() { // get next arc
         return this.next;
      }

   }

   /** Graph class.
    */

   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      public List<Arc> longest_path = Collections.synchronizedList(new LinkedList()); // represent path like List of Arc

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               if (a.target != null) { //replace
                  sb.append(" ");
                  sb.append(a.toString());
                  sb.append(" (");
                  sb.append(v.toString());
                  sb.append("->");
                  sb.append(a.target.toString());
                  sb.append(")");
               }
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      private void setGInfo(int i) {
         this.info = i;
      }

      public int getGInfo() {
         return this.info;
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], null); // replace
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) { // replace
               if (a.target != null) {
                  int j = a.target.info;
                  res[i][j]++;
               }
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, null); //replace
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Topological sort of vertices.
       * Source: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * @param source first vertex
       * @param last last vertex
       * @return longest path between two vertex
       */
      public String topolSort(String source, String last) {
         boolean cycleFound = false;
         List<Vertex> order = new ArrayList<>();

         setGInfo (1); // count number of vertices
         Vertex vit = this.first;
         while (vit.hasNext()) {
            vit.next().setVInfo (0);
            setGInfo (getGInfo() + 1);
            vit = vit.next();
         }
         // two if statement for wrong inputs
         if (Integer.parseInt(source.substring(1)) > getGInfo()){
            throw new IllegalArgumentException("Wrong source vertex!");
         }
         if (Integer.parseInt(source.substring(1)) < Integer.parseInt(last.substring(1)) ){
            throw new IllegalArgumentException("Source vertex should be bigger than last vertex!");
         }
         vit = this.first;
         while (true) {
            Arc ait = vit.first;
            while (true) {
               if (ait.target != null) {
                  Vertex v = ait.getToVert();
                  // count number of incoming edges
                  v.setVInfo(v.getVInfo() + 1);
               }
               if (!ait.hasNext()) {
                  break;
               }
               ait = ait.next();
            }
            if (!vit.hasNext()) {
               break;
            }
            vit = vit.next();
         }
         List start = Collections.synchronizedList(new LinkedList());
          vit = this.first;
         while (true) {
            if (vit.getVInfo() == 0) {
               start.add (vit); // no incoming edges
            }
             if (!vit.hasNext()) {
                 break;
             }
             vit = vit.next();
         }
         if (start.size() == 0) cycleFound = true;
         while ((!cycleFound)&(start.size() != 0)) {
            Vertex current = (Vertex)start.remove (0); // first vertex
            order.add (current);
            Arc ait = current.first;
            while (ait.target != null) {
                Vertex v = ait.getToVert();
                v.setVInfo (v.getVInfo() - 1);
                if (v.getVInfo() == 0) {
                   start.add (v); // no incoming edges anymore
                }
                if (!ait.hasNext()) {
                    break;
                }
                ait = ait.next();
            }
         }
        if (getGInfo() != order.size()) cycleFound = true;
         if (cycleFound) {
            return "Cycle was found!"; // if cycle was found then we can not do topology sort
         }
         setGInfo (0);
         return findLongestPath(order, source, last);
      } // end of topolSort()

      /**
       * Search longest path in topologically sorted list.
       * @param order List of topologically sorted Vertex
       * @param source first vertex
       * @param last last vertex
       * @return representation of path as a string.
       */
      private String findLongestPath(List<Vertex> order, String source, String last) {
         String result = "Longest directed path: ";
         Collections.reverse(order);
         List path = Collections.synchronizedList(new LinkedList());
         boolean is_start = false;
         for (Vertex v : order) {
            if (v.id.equals(last)) {
               is_start = true;
               path.add(v);
               continue;
            }
            if (is_start) {
               if (v.id.equals(source)) {
                  path.add(v);
                  Collections.reverse(path);
                  createArcPath(path); // create path of Arc for this Graph
                  result += showLongestPath(path);
                  return result;
               }
               Arc arc = v.first;
               Vertex target = (Vertex) path.get(path.size() - 1);
               while (true) {
                  if (arc.target == target) {
                     path.add(v);
                     break;
                  }
                  if (!arc.hasNext()){
                     break;
                  }
                  arc = arc.next();
               }
            }
         }
         throw new IllegalArgumentException("Can not build longest path because last vertex is higher then first vertex!");
      }

      /** Create longest path as List of Arc for Graph. */
      private void createArcPath(List<Vertex> path) {
         for (int i = 0; i < path.size() - 1; i++) {
            Vertex v = path.get(i);
            Arc arc = v.first;
            Vertex target = path.get(i + 1);
            while (true) {
               if (arc.target == target) {
                  longest_path.add(arc);
                  break;
               }
               if (!arc.hasNext()){
                  break;
               }
               arc = arc.next();
            }
         }
      }

      /** Represent longest path from List<Vertex>. */
      private String showLongestPath(List<Vertex> path) {
         String result = "";
         for (Vertex v : path) {
            if (path.get(path.size() - 1) == v){
               result += v.id;
               return result;
            }
            result += v.id + " --> ";
         }
         throw new IllegalArgumentException("Can not build longest path because last vertex is higher then first vertex!");
      }
   }
} 