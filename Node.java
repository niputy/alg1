import java.util.*;

public class Node implements java.util.Iterator<Node>{

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      setName(n);
      firstChild = null;
      nextSibling = null;
   }
   public void setName(String s) {
      name = s;
   }
   public String getName() {
      return name;
   }

   public void setFirstChild (Node r) {
      firstChild = r;
   }
   public Node getFirstChild() {
      return firstChild;
   }

   public void setNextSibling (Node d) {
      nextSibling = d;
   }
   public Node getNextSibling() {
      return nextSibling;
   }

   public Iterator<Node> children() {
      return getFirstChild();
   }

   public void addChild (Node a) {
      if (a == null) return;
      Iterator<Node> children = children();
      if (children == null)
         setFirstChild (a);
      else {
         while (children.hasNext())
            children = children.next();
         ((Node)children).setNextSibling(a);
      }
   }

   public boolean hasNext() {
      return (getNextSibling() != null);
   }

   public int size() {
      int n = 1;
      Iterator<Node> children = children();
      while (children != null) {
         n = n + ((Node)children).size();
         children = (Node)children.next();
      }
      return n;
   }
    /** Source book: Data Structures and Algorithms in Java (Michael T. Goodrich, Roberto Tamassia) page - 418.*/

   public static Node parsePostfix (String s) {
       if(s.contains(" ")) {
           throw new RuntimeException("Space In Node exception: " + s);
       }
       if(s.contains(",,")) {
           throw new RuntimeException("Two commas In Node exception: " + s);
       }
       if(s.contains("(,")) {
           throw new RuntimeException("Comma exception: " + s);
       }
       if(s.contains("()")) {
           throw new RuntimeException("Empty Subtree exception: " + s);
       }
       if(s.contains("((") && s.contains("))")) {
           throw new RuntimeException("Input with Double Brackets In Node exception: " + s);
       }
       if(s.contains("\t")) {
           throw new RuntimeException("Input with Tab In Node exception: " + s);
       }

       Stack<Node> source = new Stack<>();
       Node t = new Node(null, null, null);
       StringTokenizer tokens = new StringTokenizer(s, "(),", true);

       while(tokens.hasMoreTokens()){
           String token = tokens.nextToken().trim();

           if(token.equals("(")){
               source.push(t);
               t.firstChild = new Node(null, null, null);
               t = t.firstChild;
           }

           else if( token.equals(")")){
               t = source.pop();
           }

           else if(token.equals(",")){
               if(source.empty()) {
                   throw new RuntimeException("Comma exception: " + s);
               }

               t.nextSibling = new Node(null, null, null);
               t = t.nextSibling;
           }

           else{
               t.name = token;
           }
       }

       return t;
   }

    private static String str = "\n";
    private static int count = 1;

    public static void repeat(int num) {
        for (int i = 0; i < num; i++) {
            str += "\t";
        }
    }

    public String leftParentheticRepresentation() {
        treeToString(this);
        return str;

   }

    public static void treeToString(Node root)
    {
        // bases case
        if (root == null){
            return;
        }

        // push the root data as character


        // if leaf node, then return
        if (root.firstChild == null && root.nextSibling == null) {
            repeat(count);
            str += String.format("<L%s> ", count);
            str += root.name + " ";
            str += String.format("</L%s>\n", count);
            return;
        }

        if (root.firstChild != null) {
            repeat(count);
            str += String.format("<L%s> ", count);
            count++;
            str += root.name + "\n";
            treeToString(root.firstChild);
            count--;
            repeat(count);
            str += String.format("</L%s>\n", count);
        }

        // only if right child is present to
        // avoid extra parenthesis
        if (root.nextSibling != null)
        {
            if (root.firstChild == null) {
                repeat(count);
                str += (String.format("<L%s> ", count));
                str += root.name + " ";
                str += String.format("</L%s>\n", count);
            }
            treeToString(root.nextSibling);
        }

    }

    public Node next() {
      return getNextSibling();
   }

   public void remove() {
      throw new UnsupportedOperationException();
   }
   @Override
   public String toString() {
       return name;
      //return leftParentheticRepresentation();
   }


   public static void main (String[] param) {
      String s = "((C)B,(E,F)D,G)A";
      String gg = "((((E)D)C)B)A";
      Node t = Node.parsePostfix (gg);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
}
}